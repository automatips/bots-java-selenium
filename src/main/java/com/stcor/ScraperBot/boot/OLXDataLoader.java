package com.stcor.ScraperBot.boot;

import com.stcor.ScraperBot.Bots.model.olx.OLXCategoria;
import com.stcor.ScraperBot.Bots.model.olx.OLXCiudad;
import com.stcor.ScraperBot.Bots.model.olx.OLXFiltro;
import com.stcor.ScraperBot.Bots.model.olx.OLXOrdenamiento;
import com.stcor.ScraperBot.repository.OLXCategoriaRepository;
import com.stcor.ScraperBot.repository.OLXCiudadRepository;
import com.stcor.ScraperBot.repository.OLXFiltroRepository;
import com.stcor.ScraperBot.repository.OLXOrdenamientoRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class OLXDataLoader implements CommandLineRunner {

    @Autowired
    private OLXCategoriaRepository olxCategoriaRepository;
    
    @Autowired
    private OLXCiudadRepository olxCiudadRepository;
    
    @Autowired
    private OLXFiltroRepository olxFiltroRepository;
    
    @Autowired
    private OLXOrdenamientoRepository olxOrdenamientoRepository;

    @Autowired
    public void setOLXCategoriaRepository(OLXCategoriaRepository olxCategoriaRepository) {
        this.olxCategoriaRepository = olxCategoriaRepository;
    }

    @Autowired
    public void setOLXCiudadRepository(OLXCiudadRepository olxCiudadRepository) {
        this.olxCiudadRepository = olxCiudadRepository;
    }
    
    @Autowired
    public void setOLXFiltroRepository(OLXFiltroRepository olxFiltroRepository) {
        this.olxFiltroRepository = olxFiltroRepository;
    }
    
    @Autowired
    public void setOLXOrdenamientoRepository(OLXOrdenamientoRepository olxOrdenamientoRepository) {
        this.olxOrdenamientoRepository = olxOrdenamientoRepository;
    }

    @Override
    public void run(String... strings) throws Exception {

        ///////////////////////////////////////////////////////
        OLXCategoria cat1 = new OLXCategoria();
        cat1.setNombre("Departamentos casas en venta");
        cat1.setUrl("/departamentos-casas-venta_c367");
        cat1.setUltimoacceso(new Date());
        if (olxCategoriaRepository.findByUrl(cat1.getUrl()) == null){
            olxCategoriaRepository.save(cat1);
        }
//        OLXCategoria cat2 = new OLXCategoria();
//        cat2.setNombre("Departamentos casas en alquiler");
//        cat2.setUrl("/departamentos-casas-alquiler_c363");
//        cat2.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat2.getUrl()) == null){
//            olxCategoriaRepository.save(cat2);
//        }
//        
//        OLXCategoria cat3 = new OLXCategoria();
//        cat3.setNombre("Alquiler temporario");
//        cat3.setUrl("/alquiler-temporario_c388");
//        cat3.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat3.getUrl()) == null){
//            olxCategoriaRepository.save(cat3);
//        }
//        
//        OLXCategoria cat4 = new OLXCategoria();
//        cat4.setNombre("Inmuebles comerciales en venta");
//        cat4.setUrl("/inmuebles-comerciales-venta_c368");
//        cat4.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat4.getUrl()) == null){
//            olxCategoriaRepository.save(cat4);
//        }
//        
//        OLXCategoria cat5 = new OLXCategoria();
//        cat5.setNombre("Inmuebles comerciales en alquiler");
//        cat5.setUrl("/inmuebles-comerciales-alquiler_c1012");
//        cat5.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat5.getUrl()) == null){
//            olxCategoriaRepository.save(cat5);
//        }
//        
//        OLXCategoria cat6 = new OLXCategoria();
//        cat6.setNombre("Estacionamientos cocheras en venta");
//        cat6.setUrl("/estacionamiento-cocheras-venta_c302");
//        cat6.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat6.getUrl()) == null){
//            olxCategoriaRepository.save(cat6);
//        }
//        
//        OLXCategoria cat7 = new OLXCategoria();
//        cat7.setNombre("Estacionamientos cocheras en alquiler");
//        cat7.setUrl("/estacionamiento-cocheras-alquiler_c1063");
//        cat7.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat7.getUrl()) == null){
//            olxCategoriaRepository.save(cat7);
//        }
//        
//        OLXCategoria cat8 = new OLXCategoria();
//        cat8.setNombre("Terrenos en venta");
//        cat8.setUrl("/terrenos-venta_c410");
//        cat8.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat8.getUrl()) == null){
//            olxCategoriaRepository.save(cat8);
//        }
//        
//        OLXCategoria cat9 = new OLXCategoria();
//        cat9.setNombre("Emprendimientos en venta");
//        cat9.setUrl("/emprendimientos-venta_c1072");
//        cat9.setUltimoacceso(new Date());
//        if (olxCategoriaRepository.findByUrl(cat9.getUrl()) == null){
//            olxCategoriaRepository.save(cat9);
//        }
        ///////////////////////////////////////////////////////
        OLXCiudad ciu1 = new OLXCiudad();
        ciu1.setNombre("Córdoba Capital");
        ciu1.setUrl("/cordoba_g2006505");
        ciu1.setUltimoacceso(new Date());
        if (olxCiudadRepository.findByUrl(ciu1.getUrl()) == null){
            olxCiudadRepository.save(ciu1);
        }
        
        OLXFiltro fil1 = new OLXFiltro();
        fil1.setNombre("Filtro por moneda USD 100_to_10000");
        fil1.setUrl("filter=currency_type_eq_USD%2Cprice_between_100_to_10000");
        fil1.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil1.getUrl()) == null){
            olxFiltroRepository.save(fil1);
        }
        
        OLXFiltro fil2 = new OLXFiltro();
        fil2.setNombre("Filtro por moneda USD 10000_to_20000");
        fil2.setUrl("filter=currency_type_eq_USD%2Cprice_between_10000_to_20000");
        fil2.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil2.getUrl()) == null){
            olxFiltroRepository.save(fil2);
        }
        
        OLXFiltro fil3 = new OLXFiltro();
        fil3.setNombre("Filtro por moneda USD 20000_to_30000");
        fil3.setUrl("filter=currency_type_eq_USD%2Cprice_between_20000_to_30000");
        fil3.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil3.getUrl()) == null){
            olxFiltroRepository.save(fil3);
        }
        
        OLXFiltro fil4 = new OLXFiltro();
        fil4.setNombre("Filtro por moneda USD 30000_to_40000");
        fil4.setUrl("filter=currency_type_eq_USD%2Cprice_between_30000_to_40000");
        fil4.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil4.getUrl()) == null){
            olxFiltroRepository.save(fil4);
        }
        
        OLXFiltro fil5 = new OLXFiltro();
        fil5.setNombre("Filtro por moneda USD 40000_to_50000");
        fil5.setUrl("filter=currency_type_eq_USD%2Cprice_between_40000_to_50000");
        fil5.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil5.getUrl()) == null){
            olxFiltroRepository.save(fil5);
        }
        
        OLXFiltro fil6 = new OLXFiltro();
        fil6.setNombre("Filtro por moneda USD 50000_to_60000");
        fil6.setUrl("filter=currency_type_eq_USD%2Cprice_between_50000_to_60000");
        fil6.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil6.getUrl()) == null){
            olxFiltroRepository.save(fil6);
        }
        
        OLXFiltro fil7 = new OLXFiltro();
        fil7.setNombre("Filtro por moneda USD 60000_to_70000");
        fil7.setUrl("filter=currency_type_eq_USD%2Cprice_between_60000_to_70000");
        fil7.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil7.getUrl()) == null){
            olxFiltroRepository.save(fil7);
        }
        
        OLXFiltro fil8 = new OLXFiltro();
        fil8.setNombre("Filtro por moneda USD 70000_to_80000");
        fil8.setUrl("filter=currency_type_eq_USD%2Cprice_between_70000_to_80000");
        fil8.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil8.getUrl()) == null){
            olxFiltroRepository.save(fil8);
        }
        
        OLXFiltro fil9 = new OLXFiltro();
        fil9.setNombre("Filtro por moneda USD 80000_to_90000");
        fil9.setUrl("filter=currency_type_eq_USD%2Cprice_between_80000_to_90000");
        fil9.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil9.getUrl()) == null){
            olxFiltroRepository.save(fil9);
        }
        
        OLXFiltro fil10 = new OLXFiltro();
        fil10.setNombre("Filtro por moneda USD 90000_to_100000");
        fil10.setUrl("filter=currency_type_eq_USD%2Cprice_between_90000_to_100000");
        fil10.setUltimoacceso(new Date());
        if (olxFiltroRepository.findByUrl(fil10.getUrl()) == null){
            olxFiltroRepository.save(fil10);
        }
        ///////////////////////////////////////////////////////
        OLXOrdenamiento ord1 = new OLXOrdenamiento();
        ord1.setNombre("Creación descendente");
        ord1.setUrl("sorting=desc-creation");
        ord1.setUltimoacceso(new Date());
        if (olxOrdenamientoRepository.findByUrl(ord1.getUrl()) == null){
            olxOrdenamientoRepository.save(ord1);
        }        
        
        OLXOrdenamiento ord3 = new OLXOrdenamiento();
        ord3.setNombre("Relevancia descendente");
        ord3.setUrl("sorting=desc-relevance");
        ord3.setUltimoacceso(new Date());
        if (olxOrdenamientoRepository.findByUrl(ord3.getUrl()) == null){
            olxOrdenamientoRepository.save(ord3);
        }
        
        OLXOrdenamiento ord5 = new OLXOrdenamiento();
        ord5.setNombre("Precio ascendente");
        ord5.setUrl("sorting=asc-price");
        ord5.setUltimoacceso(new Date());
        if (olxOrdenamientoRepository.findByUrl(ord5.getUrl()) == null){
            olxOrdenamientoRepository.save(ord5);
        }
        
        OLXOrdenamiento ord6 = new OLXOrdenamiento();
        ord6.setNombre("Precio descendente");
        ord6.setUrl("sorting=desc-price");
        ord6.setUltimoacceso(new Date());
        if (olxOrdenamientoRepository.findByUrl(ord6.getUrl()) == null){
            olxOrdenamientoRepository.save(ord6);
        }
        /////////////////////////////////////////////////////
    }
}
