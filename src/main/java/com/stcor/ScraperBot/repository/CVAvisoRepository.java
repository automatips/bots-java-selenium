/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stcor.ScraperBot.repository;

import com.stcor.ScraperBot.Bots.model.cordobavende.CVAviso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface CVAvisoRepository extends JpaRepository<CVAviso,String>{
    CVAviso findByUrl(String url);
}
