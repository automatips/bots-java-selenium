package com.stcor.ScraperBot.Services;


import com.stcor.ScraperBot.Bots.model.BasicBot;
import com.stcor.ScraperBot.Bots.model.Observador;
import com.stcor.ScraperBot.Bots.model.cordobavende.CVAviso;
import com.stcor.ScraperBot.repository.CVAvisoRepository;
import org.openqa.selenium.WebElement;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;
import org.springframework.beans.factory.annotation.Autowired;
//import com.stcor.ScraperBot.repository.BasicBotRepository;


public class CordobaVende_Service implements BotService {

    @Autowired
    private CVAvisoRepository cvavisoRepository;
    
    @Autowired
    public void setCVAvisoRepository(CVAvisoRepository cvavisoRepository) {
        
        this.cvavisoRepository = cvavisoRepository;
    }
    
    @Override
    public void start() throws InterruptedException {
        BasicBot bot = new BasicBot();
        bot.InicializarWebdriver();

        //bot.IrA("https://www.cordobavende.com");
        bot.IrA("https://www.cordobavende.com/productos/38-inmuebles/lista-1-20.html?vv=lista&o=1&t=1&preciomin=&preciomax=&pr=1822&loc=1454");
        Observador observador = new Observador("//*[@id='mensajeria-interna']");
        observador.addJsExecutor("arguments[0].style.display = 'none';", observador.xpath);
        bot.AgregarObservador(observador);
        while(true){

            String categoriaActual = bot.Xpath("//div[@id='bread-crumbs']//li[1]//a//span").getText();

            bot.EsperarPorEnfocarReintentar("//ul[@class='pagination']",true,10,3);
            List<WebElement> anuncios = bot.Xpaths("//div[@id='listado-productos']//li[@class='listado']");
            for (WebElement anuncio: anuncios) {
                CVAviso aviso = new CVAviso();
                
                aviso.setTitulo(bot.Xpath(anuncio,".//div[@class='titulo-producto']//a").getText());
                aviso.setUrl(bot.Xpath(anuncio,".//div[@class='titulo-producto']//a").getAttribute("href"));
                aviso.setUsuario(bot.Xpath(anuncio,".//div[@class='info-producto']//a").getText());
                aviso.setPerfilurl(bot.Xpath(anuncio,".//div[@class='info-producto']//a").getAttribute("href"));
                String[] data = bot.Xpath(anuncio,".//div[@class='info-producto']").getText().toString().split("\n");
                aviso.setArticuloestado(data[1].split("Artículo: ")[0]);
                aviso.setUbicacion(data[2].split("Ubicación: ")[0]);
                String[] precioVisitas = bot.Xpath(anuncio,".//div[@class='precio-listado precio-gris']").getText().split("\n");
                aviso.setVisitas(precioVisitas[0].split("Visitas: ")[0]);
                aviso.setPrecio(precioVisitas[1].split(",")[0].replace(".","").replace("U$D","").replace("$",""));
                aviso.setMoneda(precioVisitas[1].split(" ")[0]);
                
                bot.Enfocar(anuncio);
                bot.NewTab(aviso.getUrl());
                bot.SiguienteTab();
                bot.EsperarPorEnfocarReintentar("//div[@id='descripcion-producto']",true,120,5);
                aviso.setDescripcion(bot.Xpath("//div[@id='descripcion-producto']//p").getText());
                
                if(bot.Xpath("//p[@class='datos-contacto']//span")!=null){
                    aviso.setContacto(bot.Xpath("//p[@class='datos-contacto']//span").getText());
                }
                
                CVAviso avisoConsulta = cvavisoRepository.findByUrl(aviso.getUrl());
                
                if (avisoConsulta==null){
                    cvavisoRepository.save(aviso);
                    System.out.println("Aviso guardado: "+aviso.getTitulo());
                }
                else{
                    System.out.println("Aviso repetido, no se guardará");
                }
                
                List<WebElement> consultasWE = bot.Xpaths("//div[@class='panel panel-default panel-consulta']//tr[@class='warning' or @class='success']");
                Map<String,String> consultas = new LinkedCaseInsensitiveMap<>();
                String tmpConsulta = "";
                String tmpRespuesta = "";
                for (WebElement consulta: consultasWE) {
                    if(consulta.getAttribute("class").equals("warning")){
                        tmpConsulta = consulta.getText();
                    }else{
                        tmpRespuesta = consulta.getText();
                        consultas.put(tmpConsulta,tmpRespuesta);
                        tmpConsulta = "";
                        tmpRespuesta = "";
                    }
                }

                bot.Close();
                bot.TabPrincipal();
            }
            
            try{
                int paginaActual = Integer.parseInt(bot.Xpath("//ul[@class='pagination']//li[@class='active']").getText());
                bot.Xpath("//ul[@class='pagination']//li//a[text()='"+(paginaActual+1)+"']").click();

            }catch (Exception ex){
                //no hay mas paginas siguientes
                //ex.printStackTrace(System.out);
                break;
            }
        }

        System.out.println("Gracias");
        System.out.println("Vuelva prontos");
    }
}
