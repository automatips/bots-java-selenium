package com.stcor.ScraperBot.Services;


import com.stcor.ScraperBot.Bots.model.BasicBot;
import com.stcor.ScraperBot.Bots.model.olx.OLXAviso;
import com.stcor.ScraperBot.Bots.model.olx.OLXCategoria;
import com.stcor.ScraperBot.Bots.model.olx.OLXCiudad;
import com.stcor.ScraperBot.Bots.model.olx.OLXFiltro;
import com.stcor.ScraperBot.Bots.model.olx.OLXOrdenamiento;
import java.util.Date;
//import com.stcor.ScraperBot.repository.BasicBotRepository;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import com.stcor.ScraperBot.repository.OLXAvisoRepository;
import com.stcor.ScraperBot.repository.OLXCategoriaRepository;
import com.stcor.ScraperBot.repository.OLXCiudadRepository;
import com.stcor.ScraperBot.boot.OLXDataLoader;
import com.stcor.ScraperBot.repository.OLXFiltroRepository;
import com.stcor.ScraperBot.repository.OLXOrdenamientoRepository;
import java.util.List;

public class OLX_Service implements BotService {

    @Autowired
    private OLXAvisoRepository olxAvisoRepository;
    
    @Autowired
    private OLXCategoriaRepository olxCategoriaRepository;
    
    @Autowired
    private OLXCiudadRepository olxCiudadRepository;
    
    @Autowired
    private OLXFiltroRepository olxFiltroRepository;
    
    @Autowired
    private OLXOrdenamientoRepository olxOrdenamientoRepository;
    
    @Autowired
    public void setOLXAvisoRepository(OLXAvisoRepository olxAvisoRepository) {
        
        this.olxAvisoRepository = olxAvisoRepository;
    }
    
    @Autowired
    public void setOLXCategoriaRepository(OLXCategoriaRepository olxCategoriaRepository) {
        
        this.olxCategoriaRepository = olxCategoriaRepository;
    }
    
    @Autowired
    public void setOLXCiudadRepository(OLXCiudadRepository olxCiudadRepository) {
        
        this.olxCiudadRepository = olxCiudadRepository;
    }
    
    @Autowired
    public void setOLXFiltroRepository(OLXFiltroRepository olxFiltroRepository) {
        this.olxFiltroRepository = olxFiltroRepository;
    }
    
    @Autowired
    public void setOLXOrdenamientoRepository(OLXOrdenamientoRepository olxOrdenamientoRepository) {
        this.olxOrdenamientoRepository = olxOrdenamientoRepository;
    }
    
    @Override
    public void start(){
        try {
            List<OLXCategoria> listaCategoria =olxCategoriaRepository.findByOrderByUltimoaccesoAsc();            
            categoriaEnUso = listaCategoria.get(0);
            
            if (listaCategoria.isEmpty()){
                OLXDataLoader odl = new OLXDataLoader();
                odl.run("");
                categoriaEnUso = listaCategoria.get(0);
            }
            else{
                categoriaEnUso.setUltimoacceso(new Date());
                olxCategoriaRepository.save(categoriaEnUso);
            }
            
            int cantidadFiltrosOrigen = (int) olxFiltroRepository.count();
            int cantidadOrdenamientosOrigen = (int) olxOrdenamientoRepository.count();            
                    
            List<OLXCiudad> listaCiudades = olxCiudadRepository.findByOrderByUltimoaccesoAsc();
            List<OLXFiltro> listaFiltros = olxFiltroRepository.findByOrderByUltimoaccesoAsc();
            List<OLXOrdenamiento> listaOrdenamientos = olxOrdenamientoRepository.findByOrderByUltimoaccesoAsc();
            
            BasicBot bot = new BasicBot();
            bot.InicializarWebdriver();
            bot.IrA(homeOlx);
            
            boolean nomeveaaas = true;
            while(nomeveaaas){//Está mal, pero no tan mal                
                listaAvisos = olxAvisoRepository.findAll();//Si, somos chingones
                
                //Armado de url bienpiola
                ciudadEnUso = listaCiudades.get(0);           
                int cantidadFiltros=cantidadFiltrosOrigen;
                int cantidadOrdenamientos=cantidadOrdenamientosOrigen;
                
                while(cantidadFiltros>0){
                    while(cantidadOrdenamientos>0){
                        //Inicia barrido de avisos encontrados
                        listaAvisosMensajear = olxAvisoRepository.findByDescartado(null);

                        //region OBTENER CELULAR
                        if (listaAvisosMensajear.size()>0){
                            for (OLXAviso p:listaAvisosMensajear) {
                                System.out.println("Abriendo "+p.getUrl());
                                bot.IrA(p.getUrl());
                                Thread.sleep(5000);
                                //Reintento para cachear atributos de frontend
                                bot.IrA(p.getUrl());
                                Thread.sleep(15000);
                                
                                //Obtener tipo de vendedor
                                try{
                                    WebElement we =
                                    bot.Xpath(".//span[@data-aut-id='value_sellertype']");
                                    p.setSellertype(we.getText());
                                }
                                catch(Exception ex){
                                    System.out.println("Error al obtener tipo de vendedor");
                                }

                                //Obtener Vendedor
                                try{
                                    WebElement we =
                                    bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[2]/div/div/div[2]/div/a/div");
                                    p.setVendedor(we.getText());
                                }
                                catch(Exception ex){
                                    System.out.println("Error al obtener Miembro Desde");
                                }
                                
                                //Obtener Localizacion
                                try{
                                    WebElement we =
                                    bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[4]/div/div[1]/span");
                                    p.setLocalizacion(we.getText());
                                }
                                catch(Exception ex){
                                    System.out.println("Error al obtener Miembro Desde");
                                }

                                //Obtener Celular
                                try{
                                    //Mostrar número
                                    WebElement we =
                                    bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[2]/div/div/div[3]/div[2]");
                                    we.click();
                                    Thread.sleep(3000);

                                    try{//Primera vez pide confirmar
                                        we =
                                        bot.Xpath("//*[@id=\"phone\"]");
                                        we.sendKeys("3518149914");

                                        we =
                                        bot.Xpath("/html/body/div/div/main/div/div/div/div[6]/div[2]/div/div[7]/button");
                                        we.click();
                                        Thread.sleep(2000);
                                        
                                        bot.EsperarPorClickear("/html/body/div/div/main/div/div/div/div[6]/div[2]/div/div/div[2]/button[1]", "\"/html/body/div/div/main/div/div/div/div[6]/div[2]/div/div/div[2]/button[1]\"", true, false);
                                        Thread.sleep(10000);
                                    }
                                    catch(Exception subex){
                                        System.out.println("No existe el mensaje de confirmación");
                                    }
                                    //Ultimo mensaje de confirmación
                                    
                                    //Leer label q tiene el celular
                                    we = bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[2]/div/div/div[3]/div");
                                    p.setCelular(we.getText());
                                    if(p.getCelular().contains("***")){
                                        we =
                                        bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[2]/div/div/div[3]/div[2]");
                                        we.click();
                                        
                                        we =
                                        bot.Xpath("/html/body/div/div/main/div/div/div/div[5]/div[2]/div/div/div[3]/div");
                                        Thread.sleep(5000);
                                        
                                        p.setCelular(we.getText());
                                        System.out.println("Aun hay asteriscos... ");
                                        break;
                                    }
                                    
                                    p.setDescartado("REVISADO");
                                    System.out.println("Celular obtenido: "+p.getCelular());
                                }
                                catch(Exception ex){
                                    System.out.println("Error al obtener Celular");
                                }
                                olxAvisoRepository.save(p);
                            }
                        }
                        //endregion                        
                        
                        filtroEnUso=listaFiltros.get(cantidadFiltros-1);
                        ordenamientoEnUso=listaOrdenamientos.get(cantidadOrdenamientos-1);
                        
                        String urlCustom = 
                                homeOlx+
                                ciudadEnUso.getUrl()+
                                categoriaEnUso.getUrl()+
                                "?"+
                                filtroEnUso.getUrl()+
                                "&"+
                                ordenamientoEnUso.getUrl();                        
                                
                        //INICIO PROCESO DENTRO DE LA PAGINA FILTRADA
                        bot.IrA(urlCustom);
                        System.out.println("Url generada: "+urlCustom);
                        
                        idAnuncio = 1;
                        cantidadErrores = 0;
                        procesoOK=true;
                        //Inicia bandera que luego dejará de marcarse en el tope de 1000
                        
                        while(procesoOK){
                            procesoOK=false;
                            boolean vContinuar = true;
                            while(vContinuar){
                                cantidadErrores=0;
                                while (cantidadErrores<2)
                                {
                                    anuncioObtenido(bot, idAnuncio);
                                    idAnuncio++;
                                }
                                vContinuar=bot.EsperarPorClickear("/html/body/div/div/main/div/section/div/div/div[4]/div[2]/div/div[3]/button", "\"/html/body/div/div/main/div/section/div/div/div[4]/div[2]/div/div[3]/button\"", true, false);
                                bot.EjecutarScript("window.scrollBy(0,900)");
                                Thread.sleep(5000);                                        
                                idAnuncio=idAnuncio-2;
                            }
                        }
                        //FIN PROCESO DENTRO DE LA PAGINA FILTRADA
                        
                        cantidadOrdenamientos--;
                    }
                    cantidadFiltros--;
                }
            }
            
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
    
    private final String homeOlx = "https://www.olx.com.ar";
    private int cantidadErrores;
    private int idAnuncio;
    private boolean procesoOK;
    private OLXCategoria categoriaEnUso;
    private OLXCiudad ciudadEnUso;
    private OLXFiltro filtroEnUso;
    private OLXOrdenamiento ordenamientoEnUso;
    private List<OLXAviso> listaAvisos;
    private List<OLXAviso> listaAvisosMensajear;
    
    public void anuncioObtenido(BasicBot bot, int vIndiceAnuncio)
    {
        try{
            WebElement we =
                bot.Xpath(
                        "/html/body/div/div/main/div/section/div/div/div[4]/div[2]/div/div[2]/ul/li[" + vIndiceAnuncio + "]/a"
                );
            OLXAviso avisoNuevo = new OLXAviso();
            avisoNuevo.setAlta(new Date());
            avisoNuevo.setUrl(we.getAttribute("href"));            
            we =
                bot.Xpath(                        
                        "/html/body/div/div/main/div/section/div/div/div[4]/div[2]/div/div[2]/ul/li[" + vIndiceAnuncio + "]/a/div/span[3]"
                );
            avisoNuevo.setTitulo(we.getText());
            System.out.println("avisoNuevo.getTitulo:"+avisoNuevo.getTitulo());
            boolean existente = false;
            for (OLXAviso p:listaAvisos) {
                if (p.getUrl().equals(avisoNuevo.getUrl())){
                    existente = true;
                    break;
                }
            }
            
            try{
                we =
                    bot.Xpath(
                            "/html/body/div/div/main/div/section/div/div/div[4]/div[2]/div/div[2]/ul/li[" + vIndiceAnuncio + "]/a/figure/img"
                    );

                avisoNuevo.setImagen(we.getAttribute("src"));
            }
            catch(Exception ex){
                System.out.println("Error cargando imagen de "+vIndiceAnuncio);
            }
            
            if(!existente){
                
                if (!avisoNuevo.getUrl().contains(homeOlx)){
                    avisoNuevo.setDescartado("AVISO EN PAGINA EXTERNA");
                }
                
                olxAvisoRepository.save(avisoNuevo);
                System.out.println("Aviso "+vIndiceAnuncio+" guardado");
            }
            else{
                System.out.println("Aviso "+vIndiceAnuncio+" preexistente en la DB");
            }
            
            cantidadErrores=0;
            procesoOK=true;
        }
        catch(Exception ex)
        {
            System.out.println("Error anuncioObtenido: "+ex.getMessage());
            cantidadErrores++;
        }
    }
}
