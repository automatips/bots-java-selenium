package com.stcor.ScraperBot.Services;

public interface BotService {
    void start() throws InterruptedException;
}
